package ro.snart.android.net;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import org.json.JSONException;
import org.json.JSONObject;
import ro.snart.android.net.API;
import ro.snart.android.net.CommitThread;
import ro.snart.android.provider.SnartContract;
import ro.snart.android.util.Adapter;

public class Syncer {

    private final Context context;

    public Syncer(Context context) {
        this.context = context;
    }

    public void asyncRefresh() {
        context.getContentResolver().delete(SnartContract.RawLink.CONTENT_URI, null, null);
        context.getContentResolver().delete(SnartContract.Station.CONTENT_URI, null, null);
        context.getContentResolver().delete(SnartContract.TransportLine.CONTENT_URI, null, null);
        context.getContentResolver().delete(SnartContract.TransportType.CONTENT_URI, null, null);

        CommitThread commitThread = new CommitThread(context);

        commitThread.submitTask(new Adapter<JSONObject, ContentValues>() {
            @Override
            public ContentValues convert(JSONObject from) throws JSONException {
                ContentValues values = new ContentValues();
                values.put(SnartContract.TransportType._ID, from.getLong("id"));
                values.put(SnartContract.TransportType.TYPE_NAME, from.getString("name"));

                return values;
            }
        }, API.EndPoint.TYPES, SnartContract.TransportType.CONTENT_URI);

        commitThread.submitTask(new Adapter<JSONObject, ContentValues>() {
            @Override
            public ContentValues convert(JSONObject from) throws JSONException {
                ContentValues values = new ContentValues();

                values.put(SnartContract.TransportLine._ID, from.getLong("id"));
                values.put(SnartContract.TransportLine.LINE_NAME, from.getString("name"));
                values.put(SnartContract.TransportLine.NIGHT_LINE, from.getString("night_line"));
                values.put(
                        SnartContract.TransportLine.TYPE_ID,
                        ContentUris.parseId(Uri.parse(from.getString("type")))
                );

                return values;
            }
        }, API.EndPoint.LINES, SnartContract.TransportLine.CONTENT_URI);

        commitThread.submitTask(new Adapter<JSONObject, ContentValues>() {
            @Override
            public ContentValues convert(JSONObject from) throws JSONException {
                ContentValues values = new ContentValues();

                values.put(SnartContract.Station._ID, from.getLong("id"));
                values.put(SnartContract.Station.STATION_NAME, from.getString("name"));
                values.put(SnartContract.Station.LATITUDE, from.getDouble("latitude"));
                values.put(SnartContract.Station.LONGITUDE, from.getDouble("longitude"));

                return values;
            }
        }, API.EndPoint.STATION, SnartContract.Station.CONTENT_URI);

        commitThread.submitTask(new Adapter<JSONObject, ContentValues>() {
            @Override
            public ContentValues convert(JSONObject from) throws JSONException {
                ContentValues values = new ContentValues();

                values.put(SnartContract.RawLink._ID, from.getLong("id"));
                values.put(SnartContract.RawLink.LINK_INDEX, from.getInt("index"));
                values.put(SnartContract.RawLink.DIRECTION, from.getString("direction"));
                values.put(SnartContract.RawLink.STREET, from.getString("street"));

                values.put(
                        SnartContract.RawLink.LINE_ID,
                        ContentUris.parseId(Uri.parse(from.getString("line")))
                );

                values.put(
                        SnartContract.RawLink.STATION_ID,
                        ContentUris.parseId(Uri.parse(from.getString("station")))
                );

                return values;
            }
        }, API.EndPoint.LINKS, SnartContract.RawLink.CONTENT_URI);

        commitThread.start();
    }

}
