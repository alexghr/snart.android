package ro.snart.android.net;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import org.json.JSONObject;
import ro.snart.android.util.Adapter;
import ro.snart.android.util.Logger;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CommitThread extends Thread {

    public static final int MESSAGE_SAVE_TO_DB = 0x54328;
    public static final int MESSAGE_FINISHED = 0x443fd;

    public static final String EXTRA_TABLE = "table";
    public static final String EXTRA_VALUES = "values";

    private static final int EXECUTOR_THREADS = 4;

    private static class ApiTask {
        public final Adapter<JSONObject, ContentValues> adapter;
        public final API.EndPoint endPoint;
        public final Uri contentUri;

        private ApiTask(Adapter<JSONObject, ContentValues> adapter, API.EndPoint endPoint, Uri contentUri) {
            this.adapter = adapter;
            this.endPoint = endPoint;
            this.contentUri = contentUri;
        }
    }

    private final WeakReference<Context> contextRef;

    private ExecutorService executor;
    private int finishedTasks = 0;
    private List<ApiTask> tasks = new ArrayList<ApiTask>();

    private long startTime;

    public CommitThread(Context context) {
        super();
        contextRef = new WeakReference<Context>(context);
    }

    @Override
    public void run() {
        Looper.prepare();

        startTime = System.currentTimeMillis();
        Logger.info("Starting...");

        Handler commitHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Context context = contextRef.get();
                if (context == null) {
                    shutdown();
                    return false;
                }

                if (msg.what == MESSAGE_FINISHED) {
                    Logger.info("Something finished...");
                    ++finishedTasks;
                    if (finishedTasks >= tasks.size()) {
                        shutdown();
                    }
                }

                if (msg.what == MESSAGE_SAVE_TO_DB) {
//                    Logger.info("Got save request...");
                    Bundle data = msg.getData();
                    Uri contentUri = data.getParcelable(EXTRA_TABLE);
                    ContentValues[] values = (ContentValues[]) data.getParcelableArray(EXTRA_VALUES);

                    context.getContentResolver().bulkInsert(contentUri, values);
                }

                return true;
            }
        });

        executor = Executors.newFixedThreadPool(EXECUTOR_THREADS);
        for (ApiTask apiTask : tasks) {
            executor.submit(new GetTask(apiTask.adapter, apiTask.endPoint, apiTask.contentUri, commitHandler));
        }

        Looper.loop();
    }

    public void shutdown() {
        Logger.info("Shutting down. It took ", (System.currentTimeMillis() - startTime) / 1000f, " seconds");

        executor.shutdown();
        executor.shutdownNow();
        Looper.myLooper().quit();
    }

    public void submitTask(Adapter<JSONObject, ContentValues> adapter, API.EndPoint endPoint, Uri contentUri) {
        if (isAlive()) {
            throw new IllegalStateException();
        }
        Logger.info("Got task for ", endPoint);
        tasks.add(new ApiTask(adapter, endPoint, contentUri));
    }
}
