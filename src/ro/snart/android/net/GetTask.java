package ro.snart.android.net;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import org.json.JSONArray;
import org.json.JSONObject;
import ro.snart.android.util.Adapter;
import ro.snart.android.util.Logger;

public class GetTask implements Runnable {

    private final Handler commitHandler;

    private final API api = new API();
    private final API.EndPoint endPoint;

    private final Uri contentUri;

    private final Adapter<JSONObject, ContentValues> adapter;

    public GetTask(Adapter<JSONObject, ContentValues> adapter,
                   API.EndPoint endPoint, Uri uri, Handler commitHandler) {
        if (adapter == null) {
            throw new NullPointerException();
        }

        this.adapter = adapter;
        this.commitHandler = commitHandler;
        this.endPoint = endPoint;
        this.contentUri = uri;
    }

    private ContentValues[] buildObjectArray(JSONArray array) throws Exception {
        ContentValues[] values = new ContentValues[array.length()];

        for (int i = 0, len = array.length(); i < len; ++i) {
            values[i] = adapter.convert(array.getJSONObject(i));
        }

        return values;
    }

    public void run() {
        int offset = 0;
        int count;

        do {
            String body = api.getEndPoint(endPoint, offset);
            if (body != null) {
                try {
                    JSONObject object = new JSONObject(body);
                    count = object.getJSONObject("meta").getInt("total_count");

                    ContentValues[] valuesToInsert = buildObjectArray(object.getJSONArray("objects"));

                    Message message = commitHandler.obtainMessage(CommitThread.MESSAGE_SAVE_TO_DB);
                    message.arg1 = valuesToInsert.length;
                    message.arg2 = count - offset;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(CommitThread.EXTRA_TABLE, contentUri);
                    bundle.putParcelableArray(CommitThread.EXTRA_VALUES, valuesToInsert);
                    message.setData(bundle);

                    message.sendToTarget();
                } catch (Exception e) {
                    Logger.error(e);
                    break;
                }
            } else {
                break;
            }

            offset += API.PAGE_SIZE;
        } while (offset < count);

        Logger.info("Finished at", offset);

        commitHandler.obtainMessage(CommitThread.MESSAGE_FINISHED).sendToTarget();
    }
}
