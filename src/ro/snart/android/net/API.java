package ro.snart.android.net;

import ro.snart.android.util.net.HttpRequest;

public class API {

    public static interface OnRequestDone {

        public void onRequestDone(String body);
    }

    public static enum EndPoint {
        TYPES("/type/"),
        LINES("/line/"),
        STATION("/station/"),
        LINKS("/link/");

        private String apiEndPoint;

        EndPoint(String apiEndPoint) {
            this.apiEndPoint = apiEndPoint;
        }

        @Override
        public String toString() {
            return apiEndPoint;
        }
    }

    private static final String USERNAME = "snart_droid";
    private static final String API_KEY = "52d64756906fba0b6b4350e53cd4c988ba83692b";
    private static final String AUTH_HEADER = String.format("ApiKey %s:%s", USERNAME, API_KEY);
    private static final String BASE_URL = "http://snart.herokuapp.com/api/v1";

    public static final int PAGE_SIZE = 200;

    private HttpRequest prepareRequest() {
        HttpRequest request = new HttpRequest();

        request.addHeader("Accept", "application/json");
        request.addHeader("Authorization", AUTH_HEADER);
        request.setMethod(HttpRequest.Method.GET);

        request.addParam("limit", String.valueOf(PAGE_SIZE));

        return request;
    }

    public String getEndPoint(EndPoint endPoint, int offset) {
        HttpRequest request = prepareRequest();
        request.setUrl(BASE_URL + endPoint);
        request.addParam("offset", String.valueOf(offset));

        return request.makeRequest().getBody();
    }

}
