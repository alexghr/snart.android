package ro.snart.android.app.activity;

import android.app.Fragment;
import android.os.Bundle;
import ro.snart.android.R;
import ro.snart.android.app.fragment.SnartRouteFragment;

public class SnartRouteActivity extends SnartActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        getFragmentManager().beginTransaction().add(
                R.id.container,
                Fragment.instantiate(this, SnartRouteFragment.class.getName(), getIntent().getExtras())
        ).commit();
    }
}