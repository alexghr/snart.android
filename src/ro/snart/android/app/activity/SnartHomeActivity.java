package ro.snart.android.app.activity;

import android.app.*;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import ro.snart.android.R;
import ro.snart.android.app.fragment.SnartExploreFragment;
import ro.snart.android.app.fragment.SnartMapFragment;

public class SnartHomeActivity extends SnartActivity implements ActionBar.OnNavigationListener {

    private Fragment mapFragment;
    private Fragment exploreFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        addFragments();

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, android.R.id.text1,
                getResources().getStringArray(R.array.navigation)
        ), this);
    }

    private void addFragments() {
        mapFragment = Fragment.instantiate(this, SnartMapFragment.class.getName());

        exploreFragment = Fragment.instantiate(this, SnartExploreFragment.class.getName(), null);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.add(R.id.container, mapFragment, "map");
        transaction.add(R.id.container, exploreFragment, "explore");

        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (itemPosition == 0) {
            ft.detach(exploreFragment);
            ft.attach(mapFragment);
            ft.commit();

            return true;
        } else if (itemPosition == 1) {
            ft.detach(mapFragment);
            ft.attach(exploreFragment);
            ft.commit();

            return true;
        }

        return false;
    }
}
