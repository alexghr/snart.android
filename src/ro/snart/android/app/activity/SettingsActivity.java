package ro.snart.android.app.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import ro.snart.android.app.fragment.SettingsFragment;

public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }
}