package ro.snart.android.app;

import android.app.Application;
import android.content.SharedPreferences;
import ro.snart.android.net.Syncer;

public class SnartApp extends Application {

    public static final String PREF_FIRST_RUN = "first_run";
    public static final String PREF_FILE = "misc";
    private Syncer sync;

    @Override
    public void onCreate() {
        super.onCreate();

        sync = new Syncer(this);

        SharedPreferences pref = getSharedPreferences(PREF_FILE, MODE_PRIVATE);
        if (pref.getBoolean(PREF_FIRST_RUN, true)) {
            sync.asyncRefresh();
        }
        pref.edit().putBoolean(PREF_FIRST_RUN, false).commit();
    }

}
