package ro.snart.android.app.fragment;

import android.app.Activity;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.util.LongSparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.*;
import ro.snart.android.R;
import ro.snart.android.app.activity.SettingsActivity;
import ro.snart.android.app.activity.SnartDetailsActivity;
import ro.snart.android.app.activity.SnartRouteActivity;
import ro.snart.android.data.DataLoader;
import ro.snart.android.data.RouteFinder;
import ro.snart.android.data.model.Link;
import ro.snart.android.data.model.Station;
import ro.snart.android.data.model.TransportLine;
import ro.snart.android.data.model.TransportType;
import ro.snart.android.provider.SnartContract;
import ro.snart.android.ui.SelectStationActionMode;
import ro.snart.android.ui.maps.StationMarker;
import ro.snart.android.util.Logger;

import java.util.*;

public class SnartMapFragment extends MapFragment implements LoaderManager.LoaderCallbacks<Cursor>,
                                                             GoogleMap.OnMarkerClickListener,
                                                             GoogleMap.OnCameraChangeListener,
                                                             SelectStationActionMode.OnCloseActionModeListener,
                                                             DataLoader.OnLoadListener {

    class ComputePath extends AsyncTask<Station, Void, List<Link>> {

        private ProgressDialog dialog;
        private Station stop;
        private Station start;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Computing path");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(List<Link> links) {
            dialog.dismiss();

            Intent intent = new Intent(getActivity(), SnartRouteActivity.class);

            long[] ids = new long[links.size()];
            for (int i = 0, len = links.size(); i < len; ++i) {
                ids[i] = links.get(i).getId();
            }

            intent.putExtra(SnartRouteFragment.ARG_LINK_IDS, ids);
            startActivity(intent);

            GoogleMap map = getMap();
            if (map == null) {
                return;
            }

            PolylineOptions opt = new PolylineOptions();
            opt.color(Color.GREEN);
            opt.width(4);

            boolean mappable = true;
            for (int i = 0, len = links.size(); mappable && i < len - 1; ++i) { // TODO use an iterator
                Link lineStart = links.get(i);
                Link lineEnd = links.get(i + 1);

                Station startStation = lineStart.getStation();
                Station endStation = lineEnd.getStation();

                if (startStation.getLatitude() == 0.0 || endStation.getLatitude() == 0.0) {
                    mappable = false;
                }

                opt.add(toMapCoords(lineStart.getStation()), toMapCoords(lineEnd.getStation()));
            }

            if (route != null) {
                route.remove();
            }

            route = null;
            if (mappable) {
                route = map.addPolyline(opt);
            }
        }

        private LatLng toMapCoords(Station station) {
            return new LatLng(station.getLatitude(), station.getLongitude());
        }

        @Override
        protected List<Link> doInBackground(Station... params) {
            start = params[0];
            stop = params[1];
            return new RouteFinder(
                    stations,
                    Integer.parseInt(
                            PreferenceManager.getDefaultSharedPreferences(getActivity())
                                             .getString(getString(R.string.settings_route_type_key), "0")
                    )
            ).calculateRoute(start, stop);
        }
    }

    private static enum ActionBarMode {
        NORMAL, SELECT_START, SELECT_END
    }

    private ActionBarMode actionBarMode = ActionBarMode.NORMAL;

    private static final double DEFAULT_BUCHAREST_LATITUDE = 44.4325;
    private static final double DEFAULT_BUCHAREST_LONGITUDE = 26.103889;
    private static final int DEFAULT_ZOOM = 14;

    private static final int STATION_LOADER_ID = 0;

    private LongSparseArray<Station> stations = null;

    private final Map<String, StationMarker> markers = new HashMap<String, StationMarker>();
    private StationMarker start, end, lastSelected;

    private double userLatitude;
    private double userLongitude;

    private GoogleMap map;
    private Polyline route;

    private BitmapDescriptor stationMarkerIcon, startStationMarkerIcon, endStationMarkerIcon;

    private long loaderTime;

    public SnartMapFragment() {
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    private static BitmapDescriptor loadMarkerIcon(Resources resources, int resId) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 3;
        Bitmap bmp = BitmapFactory.decodeResource(resources, resId, options);

        return BitmapDescriptorFactory.fromBitmap(bmp);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getLoaderManager().initLoader(STATION_LOADER_ID, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        loadUserLocation(activity);
    }

    private void loadBitmaps(Context context) {
        stationMarkerIcon = loadMarkerIcon(context.getResources(), R.drawable.marker_station);
        startStationMarkerIcon = loadMarkerIcon(context.getResources(), R.drawable.marker_station_start);
        endStationMarkerIcon = loadMarkerIcon(context.getResources(), R.drawable.marker_station_end);
    }

    private void loadUserLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null) {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (location != null) {
            userLatitude = location.getLatitude();
            userLongitude = location.getLongitude();
        } else {
            userLatitude = DEFAULT_BUCHAREST_LATITUDE;
            userLongitude = DEFAULT_BUCHAREST_LONGITUDE;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        map = getMap();
        if (map == null) {
            return;
        }

        loadBitmaps(getActivity());

        UiSettings settings = map.getUiSettings();
        settings.setCompassEnabled(false);
        settings.setMyLocationButtonEnabled(true);
        settings.setTiltGesturesEnabled(false);
        settings.setRotateGesturesEnabled(false);
        settings.setZoomControlsEnabled(false);
        settings.setZoomGesturesEnabled(true);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(userLatitude, userLongitude), DEFAULT_ZOOM));

        map.setOnMarkerClickListener(this);
        map.setOnCameraChangeListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.add(Menu.NONE, R.id.menu_select_start, Menu.NONE, R.string.action_item_select_start)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.add(Menu.NONE, R.id.menu_select_end, Menu.NONE, R.string.action_item_select_end)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.add(Menu.NONE, R.id.menu_compute, Menu.NONE, R.string.action_item_compute)
            .setIcon(android.R.drawable.ic_menu_send)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.add(Menu.NONE, R.id.menu_settings, Menu.NONE, "Settings").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_compute:
                if (start == null || end == null) {
                    Toast.makeText(getActivity(), "Select two stations", Toast.LENGTH_SHORT).show();
                } else {
                    new ComputePath().execute(start.getStation(), end.getStation());
                }
                return true;

            case R.id.menu_select_start:
                actionBarMode = ActionBarMode.SELECT_START;
                lastSelected = start;
                getActivity().startActionMode(new SelectStationActionMode(this));
                return true;

            case R.id.menu_select_end:
                actionBarMode = ActionBarMode.SELECT_END;
                lastSelected = end;
                getActivity().startActionMode(new SelectStationActionMode(this));
                return true;

            case R.id.menu_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCloseActionMode() {
        if (actionBarMode == ActionBarMode.SELECT_START) {
            start = lastSelected;
        } else if (actionBarMode == ActionBarMode.SELECT_END) {
            end = lastSelected;
        }

        actionBarMode = ActionBarMode.NORMAL;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Logger.info("Create loader");
        loaderTime = System.currentTimeMillis();
        if (i == STATION_LOADER_ID) {
            return new CursorLoader(getActivity(), SnartContract.Link.CONTENT_URI, SnartContract.Link.PROJECTION,
                                    /*SnartContract.Link.LATITUDE + "!=0"*/ null, null, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (stations == null) {
            Logger.info("Loader finished", (System.currentTimeMillis() - loaderTime) / 1000f);
            DataLoader.getInstance().loadAsync(cursor, this);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        Logger.info("loader reset");

        stations.clear();
        stations = null;

        if (map != null) {
            map.clear();
        }
    }

    @Override
    public void onFinishedLoading(LongSparseArray<TransportType> types, LongSparseArray<TransportLine> lines,
                                  LongSparseArray<Station> stations, LongSparseArray<Link> links) {

        this.stations = stations;

        for (int i = STATION_LOADER_ID, len = stations.size(); i < len; ++i) {
            Station station = stations.valueAt(i);
            StationMarker marker = new StationMarker(station);
            marker.getOptions().icon(stationMarkerIcon);
            markers.put(station.getName(), marker);
        }

        showVisibleMarkers();
    }

    private void showVisibleMarkers() {
        if (map == null) {
            return;
        }

        LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;

        for (Map.Entry<String, StationMarker> entry : markers.entrySet()) {
            StationMarker marker = entry.getValue();
            if (bounds.contains(marker.getPosition())) {
                if (!marker.isVisible()) {
                    marker.attachToMap(map);
                }
            } else {
                if (marker.isVisible()) {
                    marker.remove();
                }
            }
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        showVisibleMarkers();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        StationMarker current = markers.get(marker.getTitle());

        if (lastSelected != null && current.getMarker().equals(lastSelected.getMarker())) {
            Intent intent = new Intent(getActivity(), SnartDetailsActivity.class);
            intent.putExtra(SnartDetailsFragment.ARG_ID, current.getStation().getId());
            intent.putExtra(SnartDetailsFragment.ARG_TYPE, "station");
            startActivity(intent);
        }

        boolean selectMode = actionBarMode == ActionBarMode.SELECT_START || actionBarMode == ActionBarMode.SELECT_END;
        if (lastSelected != null && selectMode) {
            lastSelected.getOptions().icon(stationMarkerIcon);
            lastSelected.attachToMap(map);
            lastSelected.getMarker().hideInfoWindow();
        }

        if (selectMode) {
            if (actionBarMode == ActionBarMode.SELECT_START) {
                current.getOptions().icon(startStationMarkerIcon);
            } else {
                current.getOptions().icon(endStationMarkerIcon);
            }
            current.attachToMap(map);
        }

        current.getMarker().showInfoWindow();
        lastSelected = current;

        return true;
    }
}
