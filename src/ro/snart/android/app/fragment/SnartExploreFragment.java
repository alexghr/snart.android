package ro.snart.android.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import ro.snart.android.app.activity.SnartDetailsActivity;
import ro.snart.android.ui.adapter.SearchAdapter;
import ro.snart.android.util.Logger;

public class SnartExploreFragment extends SnartListFragment implements SearchView.OnQueryTextListener {

    private SearchView searchView;
    private String query;

    public SnartExploreFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        searchView = new SearchView(getActivity());
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Search for transport routes or stations.");

        menu.add(Menu.NONE, 0, Menu.NONE, android.R.string.search_go)
            .setIcon(android.R.drawable.ic_menu_search)
            .setActionView(searchView)
            .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Bundle args = new Bundle();
        args.putLong(SnartDetailsFragment.ARG_ID, id);
        Logger.info("item click type", String.valueOf(v.getTag()));
        args.putString(SnartDetailsFragment.ARG_TYPE, String.valueOf(v.getTag()));

        /*Fragment details = Fragment.instantiate(getActivity(), SnartDetailsFragment.class.getName(), args);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.hide(this);
        ft.addToBackStack("details");
        ft.add(R.id.container, details);
        ft.commit();*/

        Intent intent = new Intent(getActivity(), SnartDetailsActivity.class);
        intent.putExtra(SnartDetailsFragment.ARG_TYPE, String.valueOf(v.getTag()));
        intent.putExtra(SnartDetailsFragment.ARG_ID, id);
        startActivity(intent);
//        intent.set
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && query != null && searchView != null) {
            searchView.setQuery(query, false);
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        query = newText;
        ((SearchAdapter) getListAdapter()).getFilter().filter(newText);
        return true;
    }
}
