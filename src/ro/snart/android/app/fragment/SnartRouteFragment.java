package ro.snart.android.app.fragment;

import android.R;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import ro.snart.android.provider.SnartContract;

import java.util.ArrayList;
import java.util.List;

public class SnartRouteFragment extends SnartListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static class Container {

        public long id;
        public String stationName;
        public String lineName;
    }

    private static class Adapter extends CursorAdapter {

        public Adapter(Context context, Cursor c) {
            super(context, c, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.simple_list_item_1, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tv = (TextView) view.findViewById(R.id.text1);
            tv.setText(cursor.getString(1) + ": " + cursor.getString(2));
        }
    }

    public static final String ARG_LINK_IDS = "ro.snart.android.fragment.argument.links_ids";
    private long[] linkIds;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        linkIds = args.getLongArray(ARG_LINK_IDS);

        getLoaderManager().initLoader(0, null, this);
    }

    private static String buildWhere(int n) {
        StringBuilder builder = new StringBuilder(SnartContract.Link._ID);
        builder.append(" in(");

        for (int i = 0; i < n; ++i) {
            if (i > 0) {
                builder.append(',');
            }
            builder.append('?');
        }

        builder.append(')');

        return builder.toString();
    }

    private static String[] stringValueOf(long... ids) {
        String[] strings = new String[ids.length];
        for (int i = 0, len = ids.length; i < len; ++i) {
            strings[i] = String.valueOf(ids[i]);
        }

        return strings;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), SnartContract.Link.CONTENT_URI,
                                new String[]{
                                        SnartContract.Link._ID, SnartContract.Link.STATION_NAME,
                                        SnartContract.Link.LINE_NAME
                                },
                                buildWhere(linkIds.length), stringValueOf(linkIds), null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        LongSparseArray<Object[]> rows = new LongSparseArray<Object[]>(data.getCount());
        for (data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
            final long id = data.getLong(0);
            final String stationName = data.getString(1);
            final String lineName = data.getString(2);

            Object[] columns = new Object[3];
            columns[0] = id;
            columns[1] = stationName;
            columns[2] = lineName;

            rows.put(id, columns);
        }

        MatrixCursor cursor = new MatrixCursor(
                new String[]{
                        SnartContract.Link._ID, SnartContract.Link.STATION_NAME,
                        SnartContract.Link.LINE_NAME
                }, rows.size()
        );

        for (long id : linkIds) {
            cursor.addRow(rows.get(id));
        }

        setListAdapter(new Adapter(getActivity(), cursor));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
