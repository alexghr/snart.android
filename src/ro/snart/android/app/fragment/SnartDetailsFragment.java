package ro.snart.android.app.fragment;

import android.R;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import ro.snart.android.provider.SnartContract;

import java.util.Arrays;

public class SnartDetailsFragment extends SnartListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static class Adapter extends CursorAdapter {

        public Adapter(Context context, Cursor c) {
            super(context, c, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.simple_list_item_1, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            String[] lines = cursor.getString(2).split(",");
            Arrays.sort(lines);
            ((TextView) view.findViewById(R.id.text1)).setText(cursor.getString(1) + ": " +
                                                               Arrays.deepToString(lines).replaceAll("[\\[\\]]", ""));
        }
    }

    public static final String ARG_ID = "ro.snart.android.fragment.argument.id";
    public static final String ARG_TYPE = "ro.snart.android.fragment.argument.type";

    private long itemId;
    private String itemType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        itemId = args.getLong(ARG_ID);
        itemType = args.getString(ARG_TYPE);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (itemType.equalsIgnoreCase("station")) {
            return new CursorLoader(
                    getActivity(), SnartContract.Link.CONTENT_URI,
                    new String[]{
                            SnartContract.Link.STATION_ID + " AS _id", SnartContract.Link.STATION_NAME,
                            "group_concat("
                            + SnartContract.Link.LINE_NAME + " || " + SnartContract.Link.DIRECTION +
                            ") as lines"
                    }, SnartContract.Link.STATION_ID + " = ?", new String[]{String.valueOf(itemId)}, null);
        } else {
            return new CursorLoader(
                    getActivity(), ContentUris.withAppendedId(SnartContract.StationLines.CONTENT_URI, itemId),
                    SnartContract.StationLines.PROJECTION, null, null, null
            );
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setListAdapter(new Adapter(getActivity(), data));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
