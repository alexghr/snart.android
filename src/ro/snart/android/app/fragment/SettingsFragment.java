package ro.snart.android.app.fragment;


import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;
import ro.snart.android.R;
import ro.snart.android.util.UIUtils;

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        PreferenceManager.setDefaultValues(getActivity(), R.xml.settings, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        view.setPadding(
                view.getPaddingLeft(),
                view.getPaddingTop() + UIUtils.getActionBarHeight(getActivity()),
                view.getPaddingRight(),
                view.getPaddingBottom());
    }
}
