package ro.snart.android.app.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import ro.snart.android.ui.adapter.SearchAdapter;
import ro.snart.android.util.UIUtils;

public class SnartListFragment extends ListFragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Activity activity = getActivity();

        ListView listView = getListView();
        listView.setPadding(listView.getPaddingLeft(),
                            listView.getPaddingTop() + UIUtils.getActionBarHeight(activity),
                            listView.getPaddingRight(),
                            listView.getPaddingBottom());

        setListAdapter(new SearchAdapter(activity, null));
    }

}
