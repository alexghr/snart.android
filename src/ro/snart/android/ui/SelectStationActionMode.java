package ro.snart.android.ui;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

public class SelectStationActionMode implements ActionMode.Callback {

    public static interface OnCloseActionModeListener {

        public void onCloseActionMode();
    }

    private final OnCloseActionModeListener listener;

    public SelectStationActionMode(OnCloseActionModeListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (listener != null) {
            listener.onCloseActionMode();
        }
    }
}
