package ro.snart.android.ui.maps;

import android.text.TextUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import ro.snart.android.data.model.Link;
import ro.snart.android.data.model.Station;
import ro.snart.android.data.model.TransportLine;

import java.util.SortedSet;
import java.util.TreeSet;

public class StationMarker {

    private Marker marker;
    private MarkerOptions options;
    private final Station station;

    public StationMarker(Station station) {
        this.station = station;

        this.options = new MarkerOptions();

        this.options.position(new LatLng(station.getLatitude(), station.getLongitude()));
        refreshText();
    }

    public void refreshText() {
        String stationName = station.getName();
        String links = buildLineString(station);

        options.title(stationName).snippet(links);

        if (marker != null) {
            marker.setTitle(stationName);
            marker.setSnippet(links);

            if (marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
        }
    }

    private static String buildLineString(Station station) {
        TreeSet<String> lines = new TreeSet<String>();
        for (Link link : station.getLinks()) {
            final Link.Direction direction = link.getDirection();
            final TransportLine line = link.getLine();
            String name = line.getName() + direction.toDb();

            final String backwards = line.getName() + Link.Direction.BWD.toDb();
            final String forwards = line.getName() + Link.Direction.FWD.toDb();

            if (direction == Link.Direction.FWD && lines.contains(backwards)) {
                lines.remove(backwards);
                name += Link.Direction.BWD.toDb();
            } else if (direction == Link.Direction.BWD && lines.contains(forwards)) {
                lines.remove(forwards);
                name = forwards + Link.Direction.BWD.toDb();
            }

            lines.add(name);
        }

        return TextUtils.join(", ", lines);
    }

    public MarkerOptions getOptions() {
        return options;
    }

    public Marker getMarker() {
        return marker;
    }

    public LatLng getPosition() {
        return options.getPosition();
    }

    public boolean isVisible() {
        return marker != null && marker.isVisible();
    }

    public void remove() {
        if (marker != null) {
            marker.remove();
            marker = null;
        }
    }

    public Station getStation() {
        return station;
    }

    public Marker attachToMap(GoogleMap map) {
        if (marker != null) {
            marker.remove();
        }

        marker = map.addMarker(options);
        return marker;
    }
}
