package ro.snart.android.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.TextView;
import ro.snart.android.R;
import ro.snart.android.provider.SnartContract;

public class SearchAdapter extends CursorAdapter implements FilterQueryProvider {

    private final Context context;

    private boolean gotColumnIndices = false;
    private int nameColumn = -1;
    private int typeColumn = -1;

    public SearchAdapter(Context context, Cursor c) {
        super(context, c, 0);

        this.context = context;
        setFilterQueryProvider(this);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.search_item, parent, false);
    }

    private void getColumnIndices(Cursor cursor) {
        nameColumn = cursor.getColumnIndex(SnartContract.Query.NAME);
        typeColumn = cursor.getColumnIndex(SnartContract.Query.TYPE);

        gotColumnIndices = true;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (!gotColumnIndices) {
            getColumnIndices(cursor);
        }

        TextView itemName = (TextView) view.findViewById(R.id.search_item_name);
        ImageView itemImage = (ImageView) view.findViewById(R.id.search_item_image);

        String name = cursor.getString(nameColumn);
        String type = cursor.getString(typeColumn).toLowerCase();

        itemName.setText(name);

        if (type.equals("bus")) {
            itemImage.setImageResource(R.drawable.bus);
        } else if (type.equals("tram")) {
            itemImage.setImageResource(R.drawable.tram);
        } else if (type.equals("trolley")) {
            itemImage.setImageResource(R.drawable.trolley);
        } else if (type.equals("station")) {
            itemImage.setImageResource(R.drawable.marker_station);
        }

        view.setTag(type);
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        gotColumnIndices = false;
        return super.swapCursor(newCursor);
    }

    @Override
    public void changeCursor(Cursor cursor) {
        gotColumnIndices = false;
        super.changeCursor(cursor);
    }

    @Override
    public Cursor runQuery(CharSequence constraint) {
        if (!TextUtils.isEmpty(constraint)) {
            return context.getContentResolver().query(
                    SnartContract.Query.CONTENT_URI.buildUpon().appendPath(constraint.toString()).build(),
                    SnartContract.Query.PROJECTION, null, null, null

            );
        } else {
            return getCursor();
        }
    }
}
