package ro.snart.android.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MergeCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import ro.snart.android.provider.db.SnartDbOpener;
import ro.snart.android.util.Logger;

import java.util.Arrays;

public class SnartProvider extends ContentProvider {

    private static final int TYPES_ENDPOINT = 100;
    private static final int TYPE_ENDPOINT = 101;

    private static final int LINES_ENDPOINT = 200;
    private static final int LINE_ENDPOINT = 201;
    private static final int LINE_STATIONS_ENDPOINT = 302;

    private static final int STATIONS_ENDPOINT = 300;
    private static final int STATION_ENDPOINT = 301;

    private static final int LINKS_ENDPOINT = 400;
    private static final int LINK_ENDPOINT = 401;
    private static final int LINK_RAW_ENDPOINT = 402;
    private static final int LINKS_RAW_ENDPOINT = 403;

    private static final int QUERY_ENDPOINT = 500;

    UriMatcher uriMatcher;

    private SQLiteOpenHelper opener;

    @Override
    public boolean onCreate() {
        opener = new SnartDbOpener(getContext());

        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.TransportType.TABLE, TYPES_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.TransportType.TABLE + "/#", TYPE_ENDPOINT);

        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.TransportLine.TABLE, LINES_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.TransportLine.TABLE + "/#", LINE_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.TransportLine.TABLE +
                                                   "/" + SnartContract.StationLines.TABLE + "/#",
                          LINE_STATIONS_ENDPOINT);

        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.Station.TABLE, STATIONS_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.Station.TABLE + "/#", STATION_ENDPOINT);

        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.RawLink.TABLE, LINKS_RAW_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.RawLink.TABLE + "/#", LINK_RAW_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.Link.TABLE, LINKS_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.Link.TABLE + "/#", LINK_ENDPOINT);

        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.Query.TABLE, QUERY_ENDPOINT);
        uriMatcher.addURI(SnartContract.AUTHORITY, SnartContract.Query.TABLE + "/*", QUERY_ENDPOINT);

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int match = uriMatcher.match(uri);
        if (match == -1) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        String table = null;
        String idColumn = BaseColumns._ID;
        long id = -1;
        switch (match) {
            case TYPES_ENDPOINT:
                table = SnartContract.TransportType.TABLE;
                break;

            case TYPE_ENDPOINT:
                table = SnartContract.TransportType.TABLE;
                id = ContentUris.parseId(uri);
                break;

            case LINES_ENDPOINT:
                table = SnartContract.TransportLine.TABLE;
                break;

            case LINE_ENDPOINT:
                table = SnartContract.TransportLine.TABLE;
                id = ContentUris.parseId(uri);
                break;

            case STATIONS_ENDPOINT:
                table = SnartContract.Station.TABLE;
                break;

            case STATION_ENDPOINT:
                table = SnartContract.Station.TABLE;
                id = ContentUris.parseId(uri);
                break;

            case LINKS_ENDPOINT:
                table = SnartContract.Link.TABLE;
                break;

            case LINK_ENDPOINT:
                table = SnartContract.Link.TABLE;
                id = ContentUris.parseId(uri);
                break;

            case LINKS_RAW_ENDPOINT:
                table = SnartContract.RawLink.TABLE;
                break;

            case LINK_RAW_ENDPOINT:
                table = SnartContract.RawLink.TABLE;
                id = ContentUris.parseId(uri);
                break;

            case QUERY_ENDPOINT:
                break;
        }

        final SQLiteDatabase db = opener.getReadableDatabase();
        if (match == QUERY_ENDPOINT) {
            return queryItems(db, uri.getLastPathSegment());
        } else if (match == LINE_STATIONS_ENDPOINT) {
            return queryStations(db, ContentUris.parseId(uri));
        }

        String limit = uri.getQueryParameter("limit");

        if (id != -1 && (selection == null || selection.isEmpty() || !selection.contains(idColumn))) {
            selection = (selection == null ? "" : selection) + idColumn + "=?";

            if (selectionArgs != null) {
                selectionArgs = Arrays.copyOf(selectionArgs, selectionArgs.length + 1);
            } else {
                selectionArgs = new String[1];
            }

            selectionArgs[selectionArgs.length - 1] = String.valueOf(id);
        }


        Cursor cursor = db.query(table, projection, selection, selectionArgs, null, null, sortOrder, limit);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    private Cursor queryStations(SQLiteDatabase db, long lineId) {
        final String lineIdsColumn = "lines_ids";
        final String having = lineIdsColumn + " like '" + lineId + ",%' or " +
                              lineIdsColumn + " like '%," + lineId + ",%' or " +
                              lineIdsColumn + " like '%," + lineId + "' or " +
                              lineIdsColumn + " = '" + lineId + "'";

        Cursor stations = db.query(
                SnartContract.Link.TABLE,
                new String[]{
                        SnartContract.Link.STATION_ID + " AS " + SnartContract.StationLines._ID,
                        SnartContract.Link.STATION_NAME + " AS " + SnartContract.StationLines.STATION_NAME,
                        "group_concat(distinct " + SnartContract.Link.LINE_NAME + ") AS " +
                        SnartContract.StationLines.LINES,
                        "group_concat(distinct " + SnartContract.Link.DIRECTION + ") AS " +
                        SnartContract.StationLines.DIRECTIONS,
                        "group_concat(distinct " + SnartContract.Link.LINE_ID + ") AS " + lineIdsColumn
                }, null, null,
                SnartContract.StationLines._ID + ", " + SnartContract.StationLines.STATION_NAME,
                having, SnartContract.StationLines.DIRECTIONS + " DESC, " + SnartContract.Link.LINK_INDEX + " ASC"
        );

        return stations;
    }

    private Cursor queryItems(SQLiteDatabase db, String query) {
        final String like = " like '%' || ? || '%'";

        boolean empty = TextUtils.isEmpty(query);
        String[] args = new String[]{query};

        Cursor lines = db.query(
                SnartContract.TransportLine.TABLE + " as l, " + SnartContract.TransportType.TABLE + " as t",
                new String[]{
                        "l." + SnartContract.TransportLine._ID + " as " + SnartContract.Query._ID,
                        SnartContract.TransportLine.LINE_NAME + " as " + SnartContract.Query.NAME,
                        SnartContract.TransportType.TYPE_NAME + " as " + SnartContract.Query.TYPE
                },
                SnartContract.TransportLine.TYPE_ID + " = t." + SnartContract.TransportType._ID + " AND " +
                (empty ? null : SnartContract.TransportLine.LINE_NAME + like), empty ? null : args,
                null, null,
                SnartContract.TransportLine.LINE_NAME + " ASC"
        );

        Cursor stations = db.query(
                SnartContract.Station.TABLE,
                new String[]{
                        SnartContract.Station._ID + " as " + SnartContract.Query._ID,
                        SnartContract.Station.STATION_NAME + " as " + SnartContract.Query.NAME,
                        "'station' as " + SnartContract.Query.TYPE
                },
                empty ? null : SnartContract.Station.STATION_NAME + like, empty ? null : args,
                null, null,
                SnartContract.Station.STATION_NAME + " ASC"
        );

        return new MergeCursor(new Cursor[]{lines, stations});
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table = uriToTable(uri);
        if (table == null) {
            throw new IllegalArgumentException("URI must point to a table");
        }

        Uri item = null;

        try {
            long id = opener.getWritableDatabase().insertOrThrow(table, null, values);
            item = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(item, null);
        } catch (SQLException e) {
            Logger.error(e);
        }

        return item;
    }

    private String uriToTable(Uri uri) {
        int match = uriMatcher.match(uri);
        String table = null;

        switch (match) {
            case TYPES_ENDPOINT:
                table = SnartContract.TransportType.TABLE;
                break;

            case LINES_ENDPOINT:
                table = SnartContract.TransportLine.TABLE;
                break;

            case STATIONS_ENDPOINT:
                table = SnartContract.Station.TABLE;
                break;

            case LINKS_RAW_ENDPOINT:
                table = SnartContract.RawLink.TABLE;
                break;

            case LINKS_ENDPOINT:
                table = SnartContract.RawLink.TABLE; // defer insert/delete to link table
                break;
        }

        return table;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table = uriToTable(uri);
        if (table == null) {
            throw new IllegalArgumentException();
        }

        int modifications = opener.getWritableDatabase().delete(table, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return modifications;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public void shutdown() {
        opener.close();
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        String table = uriToTable(uri);
        if (table == null) {
            throw new IllegalArgumentException("Uri must point to a table");
        }

        boolean successful = true;
        final SQLiteDatabase db = opener.getWritableDatabase();

        DatabaseUtils.InsertHelper helper = new DatabaseUtils.InsertHelper(db, table);

        try {
            db.beginTransaction();
            for (ContentValues item : values) {
                helper.prepareForInsert();
                helper.insert(item);
            }

            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Logger.error(e);
            successful = false;
        } finally {
            db.endTransaction();
            helper.close();
        }

        if (successful) {
            getContext().getContentResolver().notifyChange(uri, null);
            return values.length;
        } else {
            return 0;
        }
    }
}
