package ro.snart.android.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class SnartContract {

    public static final String AUTHORITY = "ro.snart.android.provider";

    public static final Uri AUTHORITY_URI = new Uri.Builder()
            .scheme("content")
            .authority(AUTHORITY)
            .build();

    private static interface TransportTypeColumns {

        public static final String TYPE_NAME = "type_name";
    }

    private static interface TransportLineColumns {

        public static final String TYPE_ID = "type_id";
        public static final String LINE_NAME = "line_name";
        public static final String NIGHT_LINE = "night_line";

    }

    private static interface StationColumns {

        public static final String STATION_NAME = "station_name";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
    }

    private static interface LinkColumns {

        public static final String LINE_ID = "line_id";
        public static final String STATION_ID = "station_id";
        public static final String LINK_INDEX = "link_index";
        public static final String DIRECTION = "direction";
        public static final String STREET = "street";
    }

    private static interface LinkedStationsLinesColumns extends LinkColumns, TransportLineColumns,
                                                                StationColumns, TransportTypeColumns {

    }

    private static interface QueryColumns {

        public static final String NAME = "name";
        public static final String TYPE = "type";
    }

    private static interface StationsWithLines {

        public static final String STATION_NAME = StationColumns.STATION_NAME;
        public static final String LINES = "lines";
        public static final String DIRECTIONS = "directions";
    }

    public static final class TransportType implements TransportTypeColumns, BaseColumns {

        public static final String TABLE = "type";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE);

        public static final String[] PROJECTION = {_ID, TYPE_NAME};
    }

    public static final class TransportLine implements TransportLineColumns, BaseColumns {

        public static final String TABLE = "line";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE);

        public static final String[] PROJECTION = {_ID, TYPE_ID, LINE_NAME, NIGHT_LINE};
    }

    public static final class Station implements StationColumns, BaseColumns {

        public static final String TABLE = "station";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE);

        public static final String[] PROJECTION = {_ID, STATION_NAME, LATITUDE, LONGITUDE};

        static final String TYPES = "types";
    }

    public static final class RawLink implements LinkColumns, BaseColumns {

        public static final String TABLE = "link";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE);

        public static final String[] PROJECTION = {_ID, LINE_ID, STATION_ID, LINK_INDEX, DIRECTION, STREET};
    }

    public static final class Link implements LinkedStationsLinesColumns, BaseColumns {

        public static final String TABLE = "links_joined";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE);

        public static final String[] PROJECTION = {
                _ID, LINK_INDEX, DIRECTION, STREET, LINE_ID, LINE_NAME, TYPE_ID, TYPE_NAME,
                STATION_ID, STATION_NAME, LATITUDE, LONGITUDE};
    }

    public static final class Query implements BaseColumns, QueryColumns {

        public static final String TABLE = "query";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE);

        public static final String[] PROJECTION = {
                _ID, NAME, TYPE
        };
    }

    public static final class StationLines implements BaseColumns, StationsWithLines {

        public static final String TABLE = "stations";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(TransportLine.CONTENT_URI, TABLE);

        public static final String[] PROJECTION = {
                _ID, STATION_NAME, LINES, DIRECTIONS
        };
    }
}
