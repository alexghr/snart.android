package ro.snart.android.provider.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import ro.snart.android.provider.SnartContract;
import ro.snart.android.util.Logger;

public class SnartDbOpener extends SQLiteOpenHelper {

    private static final String DB_NAME = "snart.db";
    public static int DB_VER = 1;

    public SnartDbOpener(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Logger.info("Create database tables");
        db.execSQL(String.format(
                "create table %s(%s integer primary key, %s text not null);",
                SnartContract.TransportType.TABLE,
                SnartContract.TransportType._ID,
                SnartContract.TransportType.TYPE_NAME
        ));

        db.execSQL(String.format(
                "create table %s(%s integer primary key, %s text not null, %s boolean not null, %s integer not null);",
                SnartContract.TransportLine.TABLE,
                SnartContract.TransportLine._ID,
                SnartContract.TransportLine.LINE_NAME,
                SnartContract.TransportLine.NIGHT_LINE,
                SnartContract.TransportLine.TYPE_ID
        ));

        db.execSQL(String.format(
                "create table %s(%s integer primary key, %s real not null, %s real not null, %s text not null);",
                SnartContract.Station.TABLE,
                SnartContract.Station._ID,
                SnartContract.Station.LATITUDE,
                SnartContract.Station.LONGITUDE,
                SnartContract.Station.STATION_NAME
        ));

        db.execSQL(String.format(
                "create table %s(%s integer primary key, %s integer not null, %s text not null, %s integer not null, " +
                "%s integer not null, %s text not null);",
                SnartContract.RawLink.TABLE,
                SnartContract.RawLink._ID,
                SnartContract.RawLink.LINK_INDEX,
                SnartContract.RawLink.DIRECTION,
                SnartContract.RawLink.LINE_ID,
                SnartContract.RawLink.STATION_ID,
                SnartContract.RawLink.STREET
        ));

        db.execSQL("create view links_joined as " + sqlJoinStationsAndLines() + ";");

        /*db.execSQL("create index links_joined_station on " + SnartContract.RawLink.TABLE + " (" +
                   SnartContract.RawLink.STATION_ID + ");");
        db.execSQL("create index links_joined_line on " + SnartContract.RawLink.TABLE + " (" +
                   SnartContract.RawLink.LINE_ID + ");");*/
    }

    /* writing this and getting it right was hell */
    private static String sqlJoinStationsAndLines() {
        final String stationId = SnartContract.Station.TABLE + "." + SnartContract.Station._ID;
        final String stationName = SnartContract.Station.TABLE + "." + SnartContract.Station.STATION_NAME;
        final String stationLatitude = SnartContract.Station.TABLE + "." + SnartContract.Station.LATITUDE;
        final String stationLongitude = SnartContract.Station.TABLE + "." + SnartContract.Station.LONGITUDE;

        final String linkId = SnartContract.RawLink.TABLE + "." + SnartContract.RawLink._ID;
        final String linkStationId = SnartContract.RawLink.TABLE + "." + SnartContract.RawLink.STATION_ID;
        final String linkLineId = SnartContract.RawLink.TABLE + "." + SnartContract.RawLink.LINE_ID;
        final String linkIndex = SnartContract.RawLink.TABLE + "." + SnartContract.RawLink.LINK_INDEX;
        final String linkDirection = SnartContract.RawLink.TABLE + "." + SnartContract.RawLink.DIRECTION;
        final String linkStreet = SnartContract.RawLink.TABLE + "." + SnartContract.RawLink.STREET;

        final String lineId = SnartContract.TransportLine.TABLE + "." + SnartContract.TransportLine._ID;
        final String lineTypeId = SnartContract.TransportLine.TABLE + "." + SnartContract.TransportLine.TYPE_ID;
        final String lineName = SnartContract.TransportLine.TABLE + "." + SnartContract.TransportLine.LINE_NAME;

        final String typeId = SnartContract.TransportType.TABLE + "." + SnartContract.TransportType._ID;
        final String typeName = SnartContract.TransportType.TABLE + "." + SnartContract.TransportType.TYPE_NAME;

        StringBuilder sql = new StringBuilder("select ");

        /* Why must I specify aliases for all my columns? I'm a sad panda :< */
        sql.append(linkId).append(" as ").append(SnartContract.RawLink._ID).append(',')
           .append(linkIndex).append(" as ").append(SnartContract.RawLink.LINK_INDEX).append(',')
           .append(linkDirection).append(" as ").append(SnartContract.RawLink.DIRECTION).append(',')
           .append(linkStreet).append(" as ").append(SnartContract.RawLink.STREET).append(',');

        sql.append(linkStationId).append(" as ").append(SnartContract.RawLink.STATION_ID).append(',')
           .append(stationName).append(" as ").append(SnartContract.Station.STATION_NAME).append(',')
           .append(stationLatitude).append(" as ").append(SnartContract.Station.LATITUDE).append(',')
           .append(stationLongitude).append(" as ").append(SnartContract.Station.LONGITUDE).append(',');

        sql.append(linkLineId).append(" as ").append(SnartContract.RawLink.LINE_ID).append(',')
           .append(lineName).append(" as ").append(SnartContract.TransportLine.LINE_NAME).append(',');

        sql.append(typeId).append(" as ").append(SnartContract.Link.TYPE_ID).append(',')
           .append(typeName).append(" as ").append(SnartContract.TransportType.TYPE_NAME);

        sql.append(" from ");
        sql.append(SnartContract.Station.TABLE).append(',')
           .append(SnartContract.TransportLine.TABLE).append(',')
           .append(SnartContract.TransportType.TABLE);

        sql.append(" join ").append(SnartContract.RawLink.TABLE).append(" on(")
           .append(stationId).append("=").append(linkStationId).append(" and ")
           .append(lineId).append("=").append(linkLineId).append(" and ")
           .append(typeId).append("=").append(lineTypeId).append(')');

        sql.append(" order by ").append(stationId).append(',').append(linkDirection).append(',').append(linkIndex);

        return sql.toString();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.info("Upgrading database");
//        db.execSQL("drop table if exists " + SnartContract.RawLink.TABLE);
//        db.execSQL("drop table if exists " + SnartContract.TransportLine.TABLE);
//        db.execSQL("drop table if exists " + SnartContract.Station.TABLE);
//        db.execSQL("drop table if exists " + SnartContract.TransportType.TABLE);

//        onCreate(db);
    }
}
