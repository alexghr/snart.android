package ro.snart.android.data;

import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

public class Heap<T extends Comparable<T>> {

    public static class Node<T> {

        public T data;

        public Node<T> parent;

        public CircularList.Link<Node<T>> listLink;
        public CircularList<Node<T>> children = new CircularList<Node<T>>();

        public int degree;
        public boolean marked;

        public void addSelfToList(CircularList<Node<T>> list) {
            listLink = list.append(this);
        }

        public void makeRoot() {
            if (parent != null) {
                --parent.degree;
                parent.children.unlink(listLink);
                parent = null;
                listLink = null;
            } else {
                System.err.println("parent is null " + data);
            }
        }

        @Override
        public String toString() {
            return String.valueOf(data);
        }
    }

    private CircularList<Node<T>> roots = new CircularList<Node<T>>();
    private Node<T> min;
    private int size;

    public Node<T> insert(T t) {
        Node<T> node = new Node<T>();
        node.data = t;
        node.addSelfToList(roots);

        if (min == null || min.data.compareTo(node.data) > 0) {
            min = node;
        }

        ++size;

        return node;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void link(Node<T> parent, Node<T> child) {
        child.addSelfToList(parent.children);
        child.parent = parent;
        child.marked = false;
    }

    private void consolidate() {
        Map<Integer, Node<T>> degrees = new HashMap<Integer, Node<T>>();
        for (ListIterator<Node<T>> iterator = roots.listIterator(); iterator.hasNext(); ) {
            Node<T> node = iterator.next();
            Node<T> other = null;

            int degree = node.degree;

            while ((other = degrees.get(degree)) != null) {
                if (node.data.compareTo(other.data) > 0) {
                    Node<T> aux = node;
                    node = other;
                    other = aux;
                }

                link(node, other);
                degrees.put(degree, null);
                ++degree;
            }

            degrees.put(degree, node);
            iterator.remove();
        }

        roots.clear();
        min = null;
        for (Map.Entry<Integer, Node<T>> entry : degrees.entrySet()) {
            Node<T> node = entry.getValue();
            if (node != null) {
                node.addSelfToList(roots);
                if (min == null || min.data.compareTo(node.data) > 0) {
                    min = node;
                }
            }
        }
    }

    public T extractMin() {
        Node<T> min = this.min;

        if (min != null) {
            for (ListIterator<Node<T>> iterator = min.children.listIterator(); iterator.hasNext(); ) {
                Node<T> node = iterator.next();
                iterator.remove();
                node.parent = null;

                node.addSelfToList(roots);
            }

            Node<T> next = min.listLink.right.data;
            roots.unlink(min.listLink);
            if (min == next) {
                this.min = null;
            } else {
                this.min = next;
                consolidate();
            }

            --size;

            return min.data;
        } else {
            return null;
        }
    }

    private void cut(Node<T> child, Node<T> parent) {
        child.makeRoot();
        child.addSelfToList(roots);
        child.marked = false;
    }

    private void cascadeCut(Node<T> child) {
        final Node<T> parent = child.parent;

        if (parent != null) {
            if (!child.marked) {
                child.marked = true;
            } else {
                cut(child, parent);
                cascadeCut(parent);
            }
        }
    }

    public void decreaseKey(Node<T> node, T data) {
        if (node.data.compareTo(data) < 0) {
            throw new IllegalArgumentException();
        }

        node.data = data;
        Node<T> parent = node.parent;
        if (parent != null && node.data.compareTo(parent.data) < 0) {
            cut(node, parent);
            cascadeCut(parent);
        }

        if (min.data.compareTo(node.data) > 0) {
            min = node;
        }
    }

    public void deleteNode(Node<T> node, T minusInfinity) {
        decreaseKey(node, minusInfinity);
        extractMin();
    }

    public static <E extends Comparable<E>> Heap<E> union(Heap<E> first, Heap<E> second) {
        Heap<E> result = new Heap<E>();

        result.roots = first.roots;
        result.roots.addAll(second.roots);

        result.size = first.size + second.size;

        result.min = first.min;
        if (first.min == null || (second.min != null && first.min.data.compareTo(first.min.data) < 0)) {
            result.min = second.min;
        }

        return result;
    }

    public static <E extends Comparable<E>> Heap<E> buildHeap(E... values) {
        Heap<E> heap = new Heap<E>();
        for (E value : values) {
            heap.insert(value);
        }

        return heap;
    }

}
