package ro.snart.android.data;

import android.support.v4.util.LongSparseArray;
import ro.snart.android.data.model.Link;
import ro.snart.android.data.model.Station;
import ro.snart.android.data.model.TransportLine;
import ro.snart.android.util.Logger;

import java.util.*;

public class RouteFinder {

    public static final int ROUTE_OPTIMAL = 0;
    public static final int ROUTE_MINIMAL = 1;

    private static class DistancedStation implements Comparable<DistancedStation> {
        public Station station;
        public float distance;

        public DistancedStation(Station station) {
            this(station, Float.POSITIVE_INFINITY);
        }

        public DistancedStation(Station station, float distance) {
            this.station = station;
            this.distance = distance;
        }

        @Override
        public int compareTo(DistancedStation another) {
            return distance < another.distance ? -1 : distance == another.distance ? 0 : 1;
        }

        @Override
        public String toString() {
            return station.toString();
        }
    }

    private final LongSparseArray<Station> stations;

    private final Heap<DistancedStation> unvisited;
    private final LongSparseArray<Heap.Node<DistancedStation>> nodes;

    private final Set<Station> visited;

    private final LongSparseArray<Float> distance;
    private final LongSparseArray<Link> parents;

    private final int routeType;

    public RouteFinder(LongSparseArray<Station> stations, int type) {
        this.stations = stations;
        this.routeType = type;

        unvisited = new Heap<DistancedStation>();
        nodes = new LongSparseArray<Heap.Node<DistancedStation>>(this.stations.size());

        visited = new HashSet<Station>(this.stations.size());
        distance = new LongSparseArray<Float>(this.stations.size());
        parents = new LongSparseArray<Link>(this.stations.size());
    }

    private void init(DistancedStation startStation) {
        for (int i = 0, len = stations.size(); i < len; ++i) {
            Station station = stations.valueAt(i);

            if (startStation.station.getId() != station.getId()) {
                nodes.put(station.getId(), unvisited.insert(new DistancedStation(station)));
            }
        }

        startStation.distance = 0;
    }

    private Link findNeighbour(Link through) {
        int currentIndex = through.getIndex();
        Link.Direction direction = through.getDirection();

        for (Link link : through.getLine().getLinks()) {
            if (link.getIndex() == currentIndex + 1 && link.getDirection() == direction) {
                return link;
            }
        }

        return null; // line end
    }

    private List<Link> doBFS(Station from, Station to) {
        long start = System.currentTimeMillis();

        Queue<Station> queue = new ArrayDeque<Station>();
        visited.clear();

        visited.add(from);
        queue.add(from);
        parents.clear();

        while (!queue.isEmpty()) {
            Station current = queue.poll();
            if (current.equals(to)) {
                break;
            }
            for (Link link : current.getLinks()) {
                Link next = findNeighbour(link);
                if (next == null) {
                    continue;
                }

                Station nextStation = next.getStation();
                if (visited.contains(nextStation)) {
                    continue;
                }

                visited.add(nextStation);
                parents.put(nextStation.getId(), link);
                queue.add(nextStation);
            }
        }

        List<Link> path = new ArrayList<Link>();

        Logger.info("It took", System.currentTimeMillis() - start);
        Logger.info("From", from, "To", to);
        for (Link link = parents.get(to.getId()); link != null; link = parents.get(link.getStation().getId())) {
            Logger.info(link.getStation(), link.getLine());
            path.add(link);
        }

        Collections.reverse(path);

        Link parent = parents.get(to.getId());
        for (Link link : to.getLinks()) {
            if (parent.getLine().equals(link.getLine())) {
                path.add(link);
                break;
            }
        }

        return path;
    }

    private List<Link> doDijkstra(DistancedStation from, Station to) {
        DistancedStation current = from;
        Link prevLink;
        TransportLine prevLine;
        Float currentCost;

        init(from);

        long start = System.currentTimeMillis();

        while (!unvisited.isEmpty()) {
            currentCost = current.distance;
            prevLink = parents.get(current.station.getId());
            prevLine = prevLink == null ? null : prevLink.getLine();

            for (Link link : current.station.getLinks()) {
                Link next = findNeighbour(link);
                if (next == null || visited.contains(next.getStation())) {
                    continue;
                }

                Station nextStation = next.getStation();
                TransportLine nextLine = next.getLine();

                float cost = currentCost;

                if (prevLine == null || prevLine.equals(nextLine)) {
                    cost += 1;
                } else if (prevLine != null && prevLine.getType().equals(nextLine.getType())) {
                    cost += 2;
                } else {
                    cost += 3;
                }

                Heap.Node<DistancedStation> node = nodes.get(nextStation.getId());
                if (cost <= node.data.distance) {
                    unvisited.decreaseKey(node, new DistancedStation(nextStation, cost));
                    parents.put(nextStation.getId(), link);
                }
            }

            visited.add(current.station);

            if (current.station.equals(to)) {
                break;
            }

            current = unvisited.extractMin();
        }

        List<Link> path = new ArrayList<Link>();

        Logger.info("It took", System.currentTimeMillis() - start);
        Logger.info("From", from, "To", to, "Cost");
        for (Link link = parents.get(to.getId()); link != null; link = parents.get(link.getStation().getId())) {
            Logger.info(link.getStation(), link.getLine());
            path.add(link);
        }

        Collections.reverse(path);

        Link parent = parents.get(to.getId());
        for (Link link : to.getLinks()) {
            if (parent.getLine().equals(link.getLine())) {
                path.add(link);
                break;
            }
        }

        return path;
    }

    public List<Link> calculateRoute(Station from, Station to) {
        if (routeType == ROUTE_OPTIMAL) {
            return doDijkstra(new DistancedStation(from), to);
        } else {
            return doBFS(from, to);
        }
    }
}
