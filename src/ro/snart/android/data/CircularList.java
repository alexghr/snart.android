package ro.snart.android.data;

import java.util.AbstractSequentialList;
import java.util.ListIterator;

public class CircularList<T> extends AbstractSequentialList<T> {

    public static class Link<E> {

        public E data;
        public Link<E> left, right;

        public Link() {
            this(null, null, null);
        }

        public Link(E data) {
            this(data, null, null);
        }

        public Link(E data, Link<E> left, Link<E> right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }

        @Override
        public String toString() {
            return String.valueOf(data);
        }
    }

    private class CircularIterator implements ListIterator<T> {

        private Link<T> node, lastReturned;
        private int pos;

        public CircularIterator(int pos) {
            this.pos = pos;
            this.node = nodeAt(pos);
        }

        @Override
        public boolean hasNext() {
            return pos < size;
        }

        @Override
        public T next() {
            lastReturned = node;
            node = node.right;
            ++pos;

            return lastReturned.data;
        }

        @Override
        public boolean hasPrevious() {
            return pos > 0;
        }

        @Override
        public T previous() {
            lastReturned = node;
            node = node.left;
            --pos;

            return lastReturned.data;
        }

        @Override
        public int nextIndex() {
            return pos + 1;
        }

        @Override
        public int previousIndex() {
            return pos - 1;
        }

        private void checkLastReturned() {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
        }

        @Override
        public void remove() {
            checkLastReturned();
            unlink(lastReturned);
            --pos;
        }

        @Override
        public void set(T t) {
            checkLastReturned();
            lastReturned.data = t;
        }

        @Override
        public void add(T t) {
            insertBefore(t, node);
            ++pos;
            lastReturned = null;
        }
    }

    private int size = 0;
    private Link<T> first, last;

    @Override
    public int size() {
        return size;
    }

    public Link<T> nodeAt(int index) {
        Link<T> node;
        int pos;

        if (index >= size && size > 0) {
            index %= size;
        }

        if (index < size - index) {
            node = first;
            pos = 0;

            while (pos < index) {
                ++pos;
                node = node.right;
            }
        } else {
            node = last;
            pos = size - 1;
            while (pos > index) {
                --pos;
                node = node.left;
            }
        }

        return node;
    }

    private Link<T> buildFirst(T t) {
        first = new Link<T>(t);
        last = first;

        first.left = first;
        first.right = first;

        size = 1;

        return first;
    }

    @Override
    public boolean add(T t) {
        append(t);

        return true;
    }

    private Link<T> insertAfterNode(T t, Link<T> prev) {
        Link<T> node = new Link<T>(t, prev, prev.right);

        prev.right.left = node;
        prev.right = node;

        ++size;

        return node;
    }

    private Link<T> insertBeforeNode(T t, Link<T> next) {
        Link<T> node = new Link<T>(t, next.left, next);

        next.left.right = node;
        next.left = node;

        ++size;

        return node;
    }

    public Link<T> insertAfter(T t, Link<T> prev) {
        Link<T> node = checkBuildFirst(t);
        if (node != null) {
            return node;
        }

        node = insertAfterNode(t, prev);
        if (prev == last) {
            last = node;
        }

        return node;
    }

    public Link<T> insertBefore(T t, Link<T> next) {
        Link<T> node = checkBuildFirst(t);
        if (node != null) {
            return node;
        }

        node = insertBeforeNode(t, next);
        if (next == first) {
            first = node;
        }

        return node;
    }

    private Link<T> checkBuildFirst(T t) {
        if (first == null || last == null || (first.data == null && first.right == null && first.left == null) ||
            (last.data == null && last.right == null && last.left == null)) {
            return buildFirst(t);
        } else {
            return null;
        }
    }

    public Link<T> append(T t) {
        return insertAfter(t, last);
    }

    private T unlinkNode(Link<T> node) {
        node.left.right = node.right;
        node.right.left = node.left;
        T data = node.data;

        node.right = null;
        node.left = null;
        node.data = null;

        --size;

        return data;
    }

    private T unlinkFirst() {
        Link<T> temp = first.right;
        T data = unlinkNode(first);
        first = temp;

        return data;
    }

    private T unlinkLast() {
        Link<T> temp = last.left;
        T data = unlinkNode(last);
        last = temp;

        return data;
    }

    public T unlink(Link<T> node) {
        if (node == first) {
            return unlinkFirst();
        } else if (node == last) {
            return unlinkLast();
        } else {
            return unlinkNode(node);
        }
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new CircularIterator(index);
    }

    public void addAll(CircularList<T> list) {
        Link<T> node = append(list.first.data);
        node.right = list.first.right;
        size += list.size - 1;
    }

    @Override
    public void clear() {
        super.clear();
        size = 0;
        first = null;
        last = null;
    }

}