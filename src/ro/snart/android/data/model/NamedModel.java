package ro.snart.android.data.model;

import android.database.Cursor;

public class NamedModel extends Model {

    protected String name;

    public NamedModel() {
    }

    protected void fill(final Cursor cursor, final int idColumn, final int nameColumn) {
        super.fill(cursor, idColumn);

        if (nameColumn != -1) {
            name = cursor.getString(nameColumn);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
