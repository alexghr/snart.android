package ro.snart.android.data.model;

import android.database.Cursor;

public class Model {

    protected long id = -1;

    public Model() {

    }

    protected void fill(final Cursor cursor, final int idColumn) {
        if (idColumn != -1) {
            id = cursor.getLong(idColumn);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (this.getClass() == o.getClass() && id == ((Model) o).id);
    }

}
