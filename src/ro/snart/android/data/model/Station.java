package ro.snart.android.data.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Station extends NamedModel implements Parcelable {

    private double latitude, longitude;
    private List<Link> links = new ArrayList<Link>();

    public Station() {
        super();
    }

    public void fill(
            final Cursor cursor,
            final int idColumn,
            final int nameColumn,
            final int latitudeColumn,
            final int longitudeColumn
    ) {
        super.fill(cursor, idColumn, nameColumn);

        if (latitudeColumn != -1) {
            latitude = cursor.getDouble(latitudeColumn);
        }

        if (longitudeColumn != -1) {
            longitude = cursor.getDouble(longitudeColumn);
        }

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void addLink(Link link) {
        links.add(link);
    }

    public void addLinks(Collection<Link> links) {
        this.links.addAll(links);
    }

    @Override
    public int hashCode() {
        return (int) id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);

        if ((flags & PARCELABLE_WRITE_RETURN_VALUE) == PARCELABLE_WRITE_RETURN_VALUE) {

        }
    }

    public static final Creator<Station> CREATOR = new Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel source) {
            Station station = new Station();

            station.setId(source.readLong());
            station.setName(source.readString());
            station.setLatitude(source.readDouble());
            station.setLongitude(source.readDouble());

            return station;
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };
}
