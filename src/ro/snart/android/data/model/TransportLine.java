package ro.snart.android.data.model;

import android.database.Cursor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TransportLine extends NamedModel {

    private boolean nightLine;
    private List<Link> links = new ArrayList<Link>();
    private TransportType type;

    public TransportLine() {
        super();
    }

    @Override
    public void fill(Cursor cursor, int idColumn, int nameColumn) {
        super.fill(cursor, idColumn, nameColumn);
    }

    public boolean isNightLine() {
        return nightLine;
    }

    public void setNightLine(boolean nightLine) {
        this.nightLine = nightLine;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void addLink(Link link) {
        links.add(link);
    }

    public void addLinks(Collection<Link> links) {
        this.links.addAll(links);
    }

    public TransportType getType() {
        return type;
    }

    public void setType(TransportType type) {
        this.type = type;
    }
}
