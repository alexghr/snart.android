package ro.snart.android.data.model;

import android.database.Cursor;

public class Link extends Model {

    public static enum Direction {
        FWD("T"), BWD("R");

        private String toDb;

        Direction(String db) {
            this.toDb = db;
        }

        public String toDb() {
            return toDb;
        }

        @Override
        public String toString() {
            return toDb();
        }

        public static Direction fromDb(String db) {
            if (db.equals("T")) {
                return FWD;
            } else if (db.equals("R")) {
                return BWD;
            } else {
                return null;
            }
        }
    }

    private int index;
    private TransportLine line;
    private Station station;
    private Direction direction;

    public Link() {
    }

    public void fill(Cursor cursor,
                int idColumn,
                int lineIdColumn,
                int stationIdColumn,
                int indexColumn,
                int directionColumn) {

        super.fill(cursor, idColumn);

        if (indexColumn != -1) {
            index = cursor.getInt(indexColumn);
        }

        if (directionColumn != -1) {
            direction = Direction.fromDb(cursor.getColumnName(directionColumn));
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public TransportLine getLine() {
        return line;
    }

    public void setLine(TransportLine line) {
        this.line = line;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return station + "[" + line + ", " + direction + "]";
    }
}
