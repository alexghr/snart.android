package ro.snart.android.data;

import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.util.LongSparseArray;
import ro.snart.android.data.model.Link;
import ro.snart.android.data.model.Station;
import ro.snart.android.data.model.TransportLine;
import ro.snart.android.data.model.TransportType;
import ro.snart.android.provider.SnartContract;
import ro.snart.android.util.Logger;

import java.lang.ref.WeakReference;

public class DataLoader {

    public static interface OnLoadListener {

        public void onFinishedLoading(LongSparseArray<TransportType> types, LongSparseArray<TransportLine> lines,
                                      LongSparseArray<Station> stations, LongSparseArray<Link> links);
    }

    class LoaderTask extends AsyncTask<Cursor, Void, Void> {

        private final WeakReference<OnLoadListener> listenerWeakReference;

        public LoaderTask(OnLoadListener listener) {
            this.listenerWeakReference = new WeakReference<OnLoadListener>(listener);
        }

        @Override
        protected Void doInBackground(Cursor... params) {
            long time = System.currentTimeMillis();

            Cursor cursor = params[0];
            loadData(cursor);

            Logger.info("For", links.size(), "loading to memory", (System.currentTimeMillis() - time) / 1000f);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            OnLoadListener listener = listenerWeakReference.get();
            if (listener != null) {
                listener.onFinishedLoading(types, lines, stations, links);
            }
        }
    }

    private static DataLoader instance;

    private LoaderTask loader;

    final LongSparseArray<TransportType> types = new LongSparseArray<TransportType>();
    final LongSparseArray<TransportLine> lines = new LongSparseArray<TransportLine>();
    final LongSparseArray<Station> stations = new LongSparseArray<Station>();
    final LongSparseArray<Link> links = new LongSparseArray<Link>();

    public static DataLoader getInstance() {
        if (instance == null) {
            synchronized (DataLoader.class) {
                if (instance == null) {
                    instance = new DataLoader();
                }
            }
        }

        return instance;
    }

    static Station buildStation(Cursor cursor, int stationIdColumn, int stationNameColumn,
                                int stationLatitudeColumn,
                                int stationLongitudeColumn) {
        long id = cursor.getLong(stationIdColumn);
        String name = cursor.getString(stationNameColumn);
        double latitude = cursor.getDouble(stationLatitudeColumn);
        double longitude = cursor.getDouble(stationLongitudeColumn);

        Station station = new Station();
        station.setId(id);
        station.setLatitude(latitude);
        station.setLongitude(longitude);
        station.setName(name);

        return station;
    }

    static TransportLine buildTransportLine(Cursor cursor, int lineIdColumn, int lineNameColumn) {
        long lineId = cursor.getLong(lineIdColumn);
        String name = cursor.getString(lineNameColumn);

        TransportLine line = new TransportLine();
        line.setId(lineId);
        line.setName(name);

        return line;
    }

    static Link buildLink(Cursor cursor, int linkIdColumn, int linkDirectionColumn, int linkIndexColumn) {
        long linkId = cursor.getLong(linkIdColumn);
        int index = cursor.getInt(linkIndexColumn);
        String direction = cursor.getString(linkDirectionColumn);

        Link link = new Link();
        link.setId(linkId);
        link.setDirection(Link.Direction.fromDb(direction));
        link.setIndex(index);

        return link;
    }

    static TransportType buildTransportType(Cursor cursor, int typeIdColumn, int typeNameColumn) {
        long typeId = cursor.getLong(typeIdColumn);
        String name = cursor.getString(typeNameColumn);

        TransportType type = new TransportType();
        type.setId(typeId);
        type.setName(name);

        return type;
    }

    void loadData(Cursor cursor) {
        int stationIdColumn = cursor.getColumnIndexOrThrow(SnartContract.Link.STATION_ID);
        int stationLatitudeColumn = cursor.getColumnIndex(SnartContract.Link.LATITUDE);
        int stationLongitudeColumn = cursor.getColumnIndex(SnartContract.Link.LONGITUDE);
        int stationNameColumn = cursor.getColumnIndex(SnartContract.Link.STATION_NAME);

        int lineIdColumn = cursor.getColumnIndex(SnartContract.Link.LINE_ID);
        int lineNameColumn = cursor.getColumnIndex(SnartContract.Link.LINE_NAME);

        int typeIdColumn = cursor.getColumnIndex(SnartContract.Link.TYPE_ID);
        int typeNameColumn = cursor.getColumnIndex(SnartContract.Link.TYPE_NAME);

        int linkIdColumn = cursor.getColumnIndex(SnartContract.Link._ID);
        int linkDirectionColumn = cursor.getColumnIndex(SnartContract.Link.DIRECTION);
        int linkIndexColumn = cursor.getColumnIndex(SnartContract.Link.LINK_INDEX);

        Station station = null;
        TransportLine line = null;
        TransportType type = null;
        Link link = null;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            long stationId = cursor.getLong(stationIdColumn);

            if (station == null || station.getId() != stationId) {
                station = stations.get(stationId, null);
            }

            if (station == null) {
                station = buildStation(cursor, stationIdColumn, stationNameColumn, stationLatitudeColumn,
                                       stationLongitudeColumn);
                stations.put(stationId, station);
            }

            long lineId = cursor.getLong(lineIdColumn);

            if (line == null || line.getId() != lineId) {
                line = lines.get(lineId);
            }

            if (line == null) {
                line = buildTransportLine(cursor, lineIdColumn, lineNameColumn);
                lines.put(lineId, line);
            }

            long typeId = cursor.getLong(typeIdColumn);
            if (type == null || type.getId() != typeId) {
                type = types.get(typeId);
            }

            if (type == null) {
                type = buildTransportType(cursor, typeIdColumn, typeNameColumn);
                types.put(typeId, type);
            }

            long linkId = cursor.getLong(linkIdColumn);
            if (link == null || link.getId() != linkId) {
                link = links.get(linkId);
            }
            if (link == null) {
                link = buildLink(cursor, linkIdColumn, linkDirectionColumn, linkIndexColumn);
                links.put(linkId, link);
            }

            line.setType(type);

            link.setStation(station);
            link.setLine(line);

            station.addLink(link);
            line.addLink(link);
        }
    }

    public void loadAsync(Cursor cursor, OnLoadListener listener) {
        loader = new LoaderTask(listener);
        loader.execute(cursor);
    }

    public void load(Cursor cursor, OnLoadListener listener) {
        loadData(cursor);
        if (listener != null) {
            listener.onFinishedLoading(types, lines, stations, links);
        }
    }

    /* LongSparseArray is not thread-safe */

    public LongSparseArray<Station> getLoadedStations() {
        return loader == null || loader.getStatus() == AsyncTask.Status.RUNNING ? null : stations;
    }

    public LongSparseArray<TransportLine> getLoadedLines() {
        return loader == null || loader.getStatus() == AsyncTask.Status.RUNNING ? null : lines;
    }

    public LongSparseArray<TransportType> getLoadedTypes() {
        return loader == null || loader.getStatus() == AsyncTask.Status.RUNNING ? null : types;
    }

    public LongSparseArray<Link> getLoadedLinks() {
        return loader == null || loader.getStatus() == AsyncTask.Status.RUNNING ? null : links;
    }
}
