package ro.snart.android.util;

import android.app.Activity;
import android.util.TypedValue;

public class UIUtils {

    public static int getActionBarHeight(Activity activity) {
        TypedValue tv = new TypedValue();
        if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        } else {
            return 0;
        }
    }

}
