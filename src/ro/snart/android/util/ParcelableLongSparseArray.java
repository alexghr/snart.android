package ro.snart.android.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.util.LongSparseArray;

public class ParcelableLongSparseArray<T extends Parcelable> extends LongSparseArray<T> implements Parcelable {

    public ParcelableLongSparseArray() {
        super();
    }

    public ParcelableLongSparseArray(int initialCapacity) {
        super(initialCapacity);
    }

    public ParcelableLongSparseArray(LongSparseArray<T> array) {
        this(array.size());

        int len = array.size();
        long maxKey = Long.MIN_VALUE;
        for (int i = 0; i < len; ++i) {
            long key = array.keyAt(i);
            T value = array.valueAt(i);

            if (key > maxKey) {
                maxKey = key;
                append(key, value);
            } else {
                put(key, value);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        int len = size();
        dest.writeInt(len);

        for (int i = 0; i < len; ++i) {
            dest.writeLong(keyAt(i));
            dest.writeParcelable(valueAt(i), PARCELABLE_WRITE_RETURN_VALUE);
        }
    }

    public static final Creator<ParcelableLongSparseArray> CREATOR = new Creator<ParcelableLongSparseArray>() {
        @Override
        public ParcelableLongSparseArray createFromParcel(Parcel source) {
            int len = source.readInt();
            ParcelableLongSparseArray<Parcelable> array = new ParcelableLongSparseArray<Parcelable>(len);

            long maxKey = Long.MIN_VALUE;
            for (int i = 0; i < len; ++i) {
                long key = source.readLong();
                Parcelable value = source.readParcelable(null);

                if (key > maxKey) {
                    maxKey = key;
                    array.append(key, value);
                } else {
                    array.put(key, value);
                }
            }

            return array;
        }

        @Override
        public ParcelableLongSparseArray[] newArray(int size) {
            return new ParcelableLongSparseArray[size];
        }
    };
}
