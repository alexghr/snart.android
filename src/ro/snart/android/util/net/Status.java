package ro.snart.android.util.net;

public enum Status {
    OK, CONNECTION_ERROR, ERROR
}
