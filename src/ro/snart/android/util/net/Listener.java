package ro.snart.android.util.net;

public interface Listener<T extends Response> {
    public void onRequestFinished(T response);
}
