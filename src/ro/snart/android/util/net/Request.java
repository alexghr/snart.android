package ro.snart.android.util.net;

public interface Request<T extends Response> {

    public T makeRequest();
    public void makeAsyncRequest(final Listener<T> listener);

}
