package ro.snart.android.util.net;

import android.os.Handler;
import android.os.Message;
import ro.snart.android.util.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class HttpRequest implements Request<HttpResponse> {

    private static final String UTF8 = "UTF-8";

    private static class Worker implements Runnable {

        private final Message message;
        private final HttpRequest request;

        private Worker(Message message, HttpRequest request) {
            this.message = message;
            this.request = request;
        }

        @Override
        public void run() {
            HttpResponse response = request.makeRequest();
            message.obj = response;
            message.sendToTarget();
        }
    }

    private static class RequestHandler extends Handler {

        private Listener<HttpResponse> listener;

        private void setListener(Listener<HttpResponse> listener) {
            this.listener = listener;
        }

        @Override
        public void handleMessage(Message msg) {
            if (listener != null) {
                listener.onRequestFinished((HttpResponse) msg.obj);
            }
        }
    }

    public static enum Method {
        GET, POST
    }

    private String url;
    private Method method = Method.GET;
    private Map<String, String> headers = new HashMap<String, String>();
    private Map<String, String> params = new HashMap<String, String>();

    private byte[] buffer = new byte[32 * 1024];

    private RequestHandler handler;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    public void addParam(String key, String value) {
        try {
            params.put(URLEncoder.encode(key, UTF8), URLEncoder.encode(value, UTF8));
        } catch (UnsupportedEncodingException e) {
            Logger.error(e);
        }
    }

    private String buildUrl() {
        if (method == Method.GET && params.size() > 0) {
            StringBuilder urlBuilder = new StringBuilder(url);
            urlBuilder.append('?');

            boolean first = true;
            for (Map.Entry<String, String> param : params.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    urlBuilder.append('&');
                }
                urlBuilder.append(param.getKey()).append('=').append(param.getValue());
            }

            return urlBuilder.toString();
        } else {
            return url;
        }
    }

    private HttpURLConnection buildConnection() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(buildUrl()).openConnection();
        connection.setRequestMethod(method.toString());

        for (Map.Entry<String, String> header : headers.entrySet()) {
            connection.setRequestProperty(header.getKey(), header.getValue());
        }

        return connection;
    }

    public void makeAsyncRequest(final Listener<HttpResponse> listener) {
        if (handler == null || (handler.getLooper().getThread() != Thread.currentThread())) {
            handler = new RequestHandler();
        }

        handler.setListener(listener);
        new Thread(new Worker(handler.obtainMessage(), this)).start();
    }

    @Override
    public HttpResponse makeRequest() {
        HttpResponse response = new HttpResponse();
        try {
            HttpURLConnection connection = buildConnection();
            connection.connect();

            int responseCode = connection.getResponseCode();
            response.setHttpCode(responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                response.setStatus(Status.OK);
//                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                InputStream stream = connection.getInputStream();
                try {
                    StringBuilder builder = new StringBuilder();
                    String line;

                    /*while ((line = reader.readLine()) != null) {
                        builder.append(line).append('\n');
                    }*/
                    int len;
                    while ((len = stream.read(buffer)) != -1) {
                        builder.append(new String(buffer, 0, len));
                    }
                    response.setBody(builder.toString());

                } finally {
//                    reader.close();
                    stream.close();
                }
            } else {
                response.setStatus(Status.ERROR);
            }

        } catch (IOException e) {
            Logger.error(e);
            response.setStatus(Status.CONNECTION_ERROR);
        }

        return response;
    }
}
