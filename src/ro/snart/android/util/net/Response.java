package ro.snart.android.util.net;

public interface Response {

    public Status getStatus();

}
