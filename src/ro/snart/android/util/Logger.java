package ro.snart.android.util;

import android.text.TextUtils;
import android.util.Log;
import ro.snart.android.BuildConfig;

public class Logger {

    private final static boolean DEBUG = BuildConfig.DEBUG;

    public static void log(int priority, String tag, Object... what) {
        Log.println(priority, tag, TextUtils.join(" ", what));
    }

    private static String buildTag() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        StackTraceElement caller = stackTrace[4];

        return caller.getClassName() + "@" + caller.getMethodName() + ":" + caller.getLineNumber();
    }

    public static void info(Object... what) {
        if (!DEBUG) return;
        log(Log.INFO, buildTag(), what);
    }

    public static void debug(Object... what) {
        if (!DEBUG) return;
        log(Log.DEBUG, buildTag(), what);
    }

    public static void error(Object... what) {
        if (!DEBUG) return;
        log(Log.ERROR, buildTag(), what);
    }

    public static void error(Throwable throwable) {
        if (!DEBUG) return;
        log(Log.ERROR, buildTag(), throwable.getMessage(), '\n', Log.getStackTraceString(throwable));
    }

}
