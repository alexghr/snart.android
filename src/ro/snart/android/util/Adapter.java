package ro.snart.android.util;

import android.content.ContentValues;
import android.net.Uri;
import org.json.JSONException;
import org.json.JSONObject;

public interface Adapter<From, To> {
    public To convert(From from) throws Exception;
}
